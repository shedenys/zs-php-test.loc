<?php

# Функція для генерації хешу
function generateCode($length=6) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHI JKLMNOPRQSTUVWXYZ0123456789";
    $code = "";
    $clen = strlen($chars) - 1;
    while (strlen($code) < $length) {
        $code .= $chars[mt_rand(0,$clen)];
    }
    return $code;
}

// Флаг для ajax-обробника
$auth['status'] = false;

// З'єднуємося з БД
$db = mysqli_connect('localhost', 'root', '', 'zs-php-test_db');


if(isset($_POST['email']) && isset($_POST['password'])) {

	// Отримуємо з БД запис, у якого e-mail відповідає введенному
    $query = mysqli_query($db, "SELECT id, password, last_visit FROM users WHERE email='".mysqli_real_escape_string($db, $_POST['email'])."' LIMIT 1");
    $data = mysqli_fetch_assoc($query);

	// Порівнюємо паролі
	if($data['password'] === md5(md5($_POST['password'])))
    {
        // Генеруємо хеш
        $hash = md5(generateCode(10));

        // Записуємо в БД новий хеш авторизації БД
    	mysqli_query($db, "UPDATE users SET hash='".$hash."', last_visit='".date("Y-m-d H:i:s")."' WHERE id='".$data['id']."'");

        // Якщо користувач обрав опцію "залишатися в системі після закриття браузера"
        if(isset($_POST['remember'])) {
            $time = time()+60*60*24*30;
        }
        else {
            $time = null;
        }
        // Ставимо кукі
        setcookie("id", $data['id'], $time);
        setcookie("hash", $hash, $time);

        $auth = [
        	'status' => true,
        	'message' => 'Авторизовано. Ваш останній візит: '.$data['last_visit']
        ];
    }
    else
    {
        // Пароль введено невірно.
        // Запам'ятовуємо невдалу спробу та блокуємо користувача на 3хв, якщо кількість спроб більше 2х'
        // session_start();
        // if(isset($_SESSION['auth_ban']['count'])) {
        //     // Якщо спроб 2 або більше
        //     if($_SESSION['auth_ban']['count'] == 2) {
        //         // Встановлюємо дату і час, до якої буде діяти блокування
        //         $_SESSION['auth_ban']['exp_datetime'] = date("Y-m-d H:i:s", (time()+10));
        //     }
        //     elseif($_SESSION['auth_ban']['count'] < 2) {
        //         $_SESSION['auth_ban']['count']++;
        //     }
        // }
        // else {
        //     $_SESSION['auth_ban']['count'] = 1;
        // }

    	$auth['message'] = 'Ви ввели неправильний e-mail/пароль!';
    }
       
    echo json_encode($auth);
}

exit;
?>