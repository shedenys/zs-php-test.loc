<?php
// Реалізацію по бану від підбору паролю почав робити на сесії, але черз 1 годину зрозумів, що це не правильне рішення. Тому код закоментував.

// session_start();

require_once('authCheck.php');

$auth = authCheck();

if($auth['is_auth']) {
	// Користувача авторизовано. Виводимо інформацію про нього
	require_once('views/profile.php');
}
else {
	// Користувача не авторизовано.
	// Якщо гість заблокований, перевіряємо, його на бан
	// if(isset($_SESSION['auth_ban'])) {
	// 	$banExpDateTime = new DateTime($_SESSION['auth_ban']['exp_datetime']);
	// 	// Якщо період бана завершився, знімаємо його
	// 	if(!$banExpDateTime || date("Y-m-d H:i:s") <= $banExpDateTime) {
	// 		unset($_SESSION['auth_ban']);
	// 	}
	// }
	// // Якщо бан не знятий
	// if(isset($_SESSION['auth_ban'])) {
	// 	echo 'Ви перевіщили ліміт спроб авторизації.';
	// }
	// else {
		// Виводимо форму для авторизації
		require_once('views/login.php');
	// }
	
}
?>
